from enum import Enum
import numpy as np
from qiskit import Aer, IBMQ
from qiskit.providers.ibmq import least_busy

IBMQ.save_account('d89da5a79ec4ec34d08006048cd9a91b2ebd7811bbfd73e0651219311e38da1d950b35e35b8cee55e0b1f0c1350b31a8c6d7973a3e4c7bf1f3d5f32c07d0cbf2')

OracleType = Enum("OracleType", "BALANCED CONSTANT")

RANDOMIZE = False
TYPE = OracleType.BALANCED
if RANDOMIZE:
    TYPE =  OracleType.BALANCED if np.random.choice([True, False]) else OracleType.CONSTANT
DRAW_FINAL = False
DRAW_ORACLE = True
USE_IBM = False
BIT_COUNT = 4
SET_MAX = 2**BIT_COUNT
SET_MIN = 0
SET = []
BACKEND = Aer.get_backend('aer_simulator')
if USE_IBM:
    IBMQ.load_account()
    provider = IBMQ.get_provider(hub='ibm-q')
    BACKEND = least_busy(provider.backends(filters=lambda x: x.configuration().n_qubits >= (BIT_COUNT + 1) and
                                                             not x.configuration().simulator and x.status().operational == True))
    print("least busy backend: ", BACKEND)