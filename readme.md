# Qiskit Demo
Hello!  Here's some code I did for a short talk I gave on quantum computing.

It's mostly code for building oracles and solutions required by the [Deutsch–Jozsa 
algorithm](https://en.wikipedia.org/wiki/Deutsch%E2%80%93Jozsa_algorithm).  Initially I implimented a non-quantum oracle in the classical file, 
though for the quantum solution to work the oracle must also be a quantum oracle.  Main hosts a classical solution and initially I 
also planned to place the quantum solution there so it would be plausable to run either from one place against the same oracle. However as qiskits 
simulation was explicitily designed to include some realistic noise when simulating circuits it became apparent that the classical solution would 
not run against a quantum oracle so I moved to just doing implimentations in quantum.py.

Check out [qiskit](https://qiskit.org/) for more info on quantum circuits and how their simulation works.
