import random
import constants

function_set = []

def gen_ones():
    ones = []
    for i in range(constants.SET_MAX // 2):
        ones.append(1)
    return ones

def gen_zeros():
    zeros = []
    for i in range(constants.SET_MAX // 2):
        zeros.append(0)
    return zeros


def create_randomised_balanced_array():
    list_ones = gen_ones()
    list_zeros = gen_zeros()
    for i in range(32):
        constants.SET.append(i)
        if len(list_ones) > 0 and len(list_zeros) > 0:
            function_set.append(list_ones.pop() if random.getrandbits(1) else list_zeros.pop())
        elif len(list_ones) > 0:
            function_set.append(list_ones.pop())
        else:
            function_set.append(list_zeros.pop())

def create_randomised_constant_array():
    number = random.randint(0, 1)
    for i in range(32):
        constants.SET.append(i)
        function_set.append(number)


def function_setup():
    if constants.RANDOMIZE:
        constants.BALANCED = random.getrandbits(1)

    if constants.TYPE :
        create_randomised_balanced_array()
    else:
        create_randomised_constant_array()

    oracle = create_randomised_balanced_array() \
        if constants.TYPE == constants.OracleType.BALANCED \
        else create_randomised_constant_array()

    print(function_set)

def function(value):
    return function_set[value]

def count(array):
    counter = 0
    for x in array:
        counter+=x
    return counter
