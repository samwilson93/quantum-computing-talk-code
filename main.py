import classical
import constants


def classical_solution():
    counter = 0
    previous = None
    solved = False
    while not solved:
        number = constants.SET.pop(0)
        value = classical.function(number)
        counter += 1
        if previous is None:
            previous = value
        else:
            if previous is value and counter <= (constants.SET_MAX // 2):
                previous = value
            elif previous is not value:
                solved = True
                print("The function is balanced")
            else:
                solved = True
                print("The function is constant")
    print("It took {} operations".format(counter))

if __name__ == '__main__':
    classical.function_setup()
    classical_solution()

