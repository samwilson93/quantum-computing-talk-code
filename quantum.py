# initialization
import numpy as np
import constants

# importing Qiskit
from qiskit import QuantumCircuit, assemble, transpile

# import basic plot tools
from qiskit.visualization import plot_histogram

def constant_oracle():
    print("Constant")
    const_oracle = QuantumCircuit(constants.BIT_COUNT + 1)

    # First decide what the fixed output of the oracle will be
    # (either always 0 or always 1)
    output = np.random.randint(0, 2)
    if output == 1:
        const_oracle.x(constants.BIT_COUNT)

    return const_oracle

def balanced_oracle(random_number=None):
    # print("Balanced")
    new_oracle = QuantumCircuit(constants.BIT_COUNT + 1)

    # First generate a random number that tells us which CNOTs to
    # wrap in X-gates:
    if random_number is None:
        random_number = np.random.randint(1, constants.SET_MAX)
    # Next, format 'random_number' as a binary string of length 'constants.BIT_COUNT', padded with zeros:
    random_number_bin = np.binary_repr(random_number, constants.BIT_COUNT)
    # print(random_number_bin)
    # Next, we place the first X-gates. Each digit in our binary string
    # corresponds to a qubit, if the digit is 0, we do nothing, if it's 1
    # we apply an X-gate to that qubit:
    for index, value in enumerate(random_number_bin):
        if value == '1':
            new_oracle.x(index)


    # Do the controlled-NOT gates for each qubit, using the output qubit
    # as the target:
    for index in range(constants.BIT_COUNT):
        new_oracle.cx(index, constants.BIT_COUNT)

    # Next, place the final X-gates
    for index, value in enumerate(random_number_bin):
        if value == '1':
            new_oracle.x(index)

    return new_oracle

def get_oracle():
    oracle = balanced_oracle() if constants.TYPE == constants.OracleType.BALANCED else constant_oracle()
    if constants.DRAW_ORACLE:
        oracle.draw().show()
    return oracle


def get_oracle_gate():
    oracle = balanced_oracle() if constants.TYPE == constants.OracleType.BALANCED else constant_oracle()
    if constants.DRAW_ORACLE:
        oracle.draw().show()
    gate = oracle.to_gate()
    gate.name = "Oracle"
    return gate


def build_dj(oracle):
    new_circuit = QuantumCircuit(constants.BIT_COUNT + 1, constants.BIT_COUNT + 1)
    # Set up the output qubit:
    new_circuit.x(constants.BIT_COUNT)

    # And set up the input register:
    for qubit in range(constants.BIT_COUNT + 1):
        new_circuit.h(qubit)

    # Let's append the oracle gate to our circuit:
    complete = new_circuit.compose(oracle)

    # Finally, perform the H-gates again and measure:
    for qubit in range(constants.BIT_COUNT):
        complete.h(qubit)

    for i in range(constants.BIT_COUNT + 1):
        complete.measure(i, i)

    return complete

def function(number, oracle):

    new_circuit = QuantumCircuit(constants.BIT_COUNT+1, constants.BIT_COUNT + 1)

    bits = np.binary_repr(number, constants.BIT_COUNT)

    # print(bits)

    # And set up the input register:
    for i, bit in enumerate(bits):
        if bit == '1':
            new_circuit.x(i)

    for qubit in range(constants.BIT_COUNT + 1):
        new_circuit.h(qubit)

    new_circuit.x(constants.BIT_COUNT)


    new_circuit.barrier()

    combined = new_circuit.compose(oracle)

    combined.barrier()

    # Finally, perform the H-gates again and measure:
    for i in range(constants.BIT_COUNT):
        combined.h(i)

    for i in range(constants.BIT_COUNT + 1):
        combined.measure(i, i)


    if constants.DRAW_FINAL:
        combined.draw().show()

    combined_circuit = transpile(combined, constants.BACKEND)
    assembled = assemble(combined_circuit)

    running = constants.BACKEND.run(assembled, shots=1, memory=True)
    return running.result().get_counts()

if __name__ == '__main__':
    # print(constants.BACKEND)
    # for i in range(1, 32):
    #     box = balanced_oracle(i)
    #     ones = 0
    #     for count in range(constants.SET_MAX):
    #         result = function(count, box)
    #         if list(result.keys())[0][0] == '1':
    #             ones+=1
    #     if ones == 16:
    #         print(i)
    #         print(box.draw())

    # dj_circuit =build_dj(get_oracle_gate())
    #
    #
    # transpiled_dj_circuit = transpile(dj_circuit, constants.BACKEND, optimization_level=3)
    # job = constants.BACKEND.run(transpiled_dj_circuit)
    # job_monitor(job, interval=2)
    #
    # # Get the results of the computation
    # results = job.result()
    # answer = results.get_counts()
    #
    # plot_histogram(answer)
    box = get_oracle()

    dj_circuit = build_dj(box)
    dj_circuit.draw().show()

    transpiled = transpile(dj_circuit, constants.BACKEND)
    assembled_circuit = assemble(transpiled)
    job = constants.BACKEND.run(assembled_circuit, shots=1, memory=True)
    answer = job.result().get_counts()
    print(answer)
    plot_histogram(answer).show()